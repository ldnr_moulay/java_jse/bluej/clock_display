/**
 * Simule l'affichage d'une montre digitale
 * L'affichage est simul� par la valeur de la propri�t� "myDisplayString"
 */
public class ClockDisplay
{
    private String myDisplayString;

    // m�thode qui retourne myDisplayString
    private NumberDisplay heure;
    private NumberDisplay minutes;

    /**
     * Constructor for objects of class ClockDisplay
     * Initialisation avec des heures / minutes par d�faut
     */
    public ClockDisplay()
    {
        // Heure entre 00:00 et 23:59
        heure = new NumberDisplay(23,24);
        minutes = new NumberDisplay(58,60);
        raffraichirDisplay();
    }

    /**
     * Initialise un ClockDisplay avec les valeurs sp�cifi�es
     * @param hours : la valeur des heures 
     * @param minutes : la valeur des minutes 
     */
    public ClockDisplay(int h, int m) {
        heure = new NumberDisplay(h,24);
        minutes = new NumberDisplay(m,60);
        raffraichirDisplay();
    }
    private void raffraichirDisplay()
    {
        myDisplayString = heure.formatNombre(heure.getValue()) + ":" + minutes.formatNombre(minutes.getValue());
    }

    /**
     *  Simule le passage d'une minute
     */
    public void timeTick()
    {   minutes.incrementer();
        if(minutes.getValue() == 0)
            heure.incrementer();
        raffraichirDisplay();
    }

    /**
     * Getter de la propri�t� myDisplayString
     */
    public String getMyDisplayString()
    {
        return myDisplayString;
    }
}
