
/**
 * D�crivez votre classe NumberDisplay ici.
 *
 * @author (votre nom)
 * @version (un num�ro de version ou une date)
 */
public class NumberDisplay
{
    // variables d'instance - remplacez l'exemple qui suit par le v�tre
    private int value;
    private int limit;

    /**
     * Constructeur d'objets de classe NumberDisplay
     */
    public NumberDisplay(int value, int limit)
    {
        if (value < 0 || value >= limit)
            value = 0;
        this.value=value;
        this.limit=limit;
    }

    /**
     * Un exemple de m�thode - remplacez ce commentaire par le v�tre
     *
     * @param  y   le param�tre de la m�thode
     * @return     la somme de x et de y
     */
    public void incrementer()
    {
        value = value+1;
        if(value>=limit)
            value=0;
     
    }
    public String formatNombre(int nb)
    {
        // return (nb < 10) ? "0" + nb : "0" + nb;
        
          if (nb < 10)
            return "0" + nb;
          else
            return "" + nb;
    }
    
    public int getValue(){
        return value;
    }
}
